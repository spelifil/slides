# Heights of US presidents
example of aggregation functions

## Importing libraries


```python
import pandas as pd
import numpy as np
```

> in case you do not have requiered libraries you can install them with pip and the requirements.txt

```pip install -r requirements.txt```

## Importing and preparing data


```python
data = pd.read_csv("presidents_data.csv")
heights = np.array(data["height(cm)"])
# print(data["height(cm)"])
print(heights)
```

## Computing statistics


```python
print("Mean height:        ", heights.mean())
print("Standard deviation: ", heights.std())
print("Minimum height:     ", heights.min())
print("Maximum height:     ", heights.max())
```


```python
print("25th percentile: ", np.percentile(heights, 25))
print("Median:          ", np.median(heights))
print("75th percentile: ", np.percentile(heights, 75))
```


```python
## Plotting a graph
```


```python
import matplotlib.pyplot as plt
import seaborn; seaborn.set() # set plot style

plt.hist(heights)
plt.title('Height Distribution of US Presidents')
plt.xlabel('height (cm)')
plt.ylabel('number')
```
