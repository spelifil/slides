import numpy as np
import pandas as pd
import time

data = pd.read_csv('AV_AN_WAGE.csv')
salary = np.array(data['Value'])
#print (salary)

#Task 1: calculate the sum of all OECD sallaries with basic Pythonfunction
print ("sum of all salaries with basic Pythonfunction:", sum(salary))

#task 2: calculate the sum of all OECD salaries with the NumPy's version
print ("sum of all salaries with NumPy:", np.sum(salary))

#calculate the requiered time to determine the max- salary with the basic Pythonfunction
start_time = time.time()
max(salary)
end_time = time.time()
print ("time to complete max_salary with normal Python function:", end_time - start_time)

#calculate the requierd time to determine the max- salary with the NumPy's version
start_time2 = time.time()
np.max(salary)
end_time2 = time.time()
print ("time to complete max_salary with NumPys:", end_time2 - start_time2)

#create 3x3 matrix with the first 9 salaries
M = salary[:9].reshape(3,3)
print ("3x3 matrix with first salaries:\n", M)

print ("3x3 matrix with first salaries:\n", M.round(-1))

#calculate the smalles ammount in each column 
print ("the smallest ammount of each column:", M.min(axis=0))

#print the highest ammount in each row
print ("the highest ammount of each row:" , M.max(axis=1))

#print the average salary of all average sallaries
print ("Avg Salary:", salary.mean())

#print 75th percentile: 
print ("75th percentile:", np.percentile(salary, 75))

