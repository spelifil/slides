---
marp: true
theme: uncover
class: default
paginate: false

---

## NaN-safe variants

- IEEE floating-point NaN
- not-a-number
- ignores missing values


---

![bg fit](table_functions.png)

---
# What is the Average Height of US Presidents?

An example of using aggregates in NumPy

---

## The data set

*/data/president_heights.csv*

```csv
order,name,height(cm)
1,George Washington,189
2,John Adams,170
3,Thomas Jefferson,189
```

---

## Reading data

- Pandas package


```python
import pandas as pd
data = pd.read_csv('data/president_heights.csv')
heights = np.array(data['height(cm)'])
print(heights)
```
out: `[189 170 189 163 183 171 185 168 173 183 173 173 175 178 183 193 178 173
174 183 183 168 170 178 182 180 183 178 182 188 175 179 183 193 182 183
177 185 188 188 182 185]`

---

## Computing statistics

```python
print("Mean height:        ", heights.mean())
print("Standard deviation: ", heights.std())
print("Minimum height:     ", heights.min())
print("Maximum height:     ", heights.max())
```

```
Mean height:         179.738095238
Standard deviation:  6.93184344275
Minimum height:      163
Maximum height:      193
```

---

```python
print("25th percentile: ", np.percentile(heights, 25))
print("Median:          ", np.median(heights))
print("75th percentile: ", np.percentile(heights, 75))
```

```
25th percentile:
 174.25
Median:
 182.0
75th percentile:
 183.0
```

---
## Matplotlib

```python
import matplotlib.pyplot as plt
import seaborn; seaborn.set() # set plot style

plt.hist(heights)
plt.title('Height Distribution of US Presidents')
plt.xlabel('height (cm)')
plt.ylabel('number')
```

![bg right 110%](president_graph.png)
<!-- ![bg right:50% w:150%](president_graph.png) -->